# /packages/intranet-trans-project-wizard/tcl/intranet-trans-project-wizard.tcl

ad_library {
    Component for translation project wizard

    @author frank.bergmann@project-open.com
    @creation-date 10 June 2006
}


# ------------------------------------------------------
# Main Wizard Component
# ------------------------------------------------------

ad_proc im_trans_project_wizard_component { 
    -project_id:required
    {-user_id ""}
} {
    Returns a formatted HTML table representing the status of the translation project
} {
    if {![im_project_has_type $project_id "Translation Project"]} { return "" }


    if {$user_id eq ""} {set user_id [ad_conn user_id]}

    im_project_permissions $user_id $project_id view read write admin

    if {!$write} { return "" }

    set params [list [list project_id $project_id] [list user_id $user_id]]

    set component_path [parameter::get_from_package_key -package_key "intranet-trans-project-wizard" -parameter "ComponentPath"]

    set result [ad_parse_template -params $params $component_path]
    return $result
}
