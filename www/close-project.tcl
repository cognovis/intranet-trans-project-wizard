# /packages/intranet-trans-project-wizard/www/close-project.tcl
#
# Copyright (c) 2011, cognovís GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
#
 
ad_page_contract {
    
    Quickly close a project
    @author Malte Sussdorff (malte.sussdorff@cognovis.de)
    @creation-date 2016-10-18
} {
    {project_id ""}
}

# -----------------------------------------------------------
# Defaults
# -----------------------------------------------------------
set user_id [ad_maybe_redirect_for_registration]

    
set perm_p 0
set quote_id ""

# Check if the user has admin rights on the parent_id
# to allow freelancers to add sub-projects
im_project_permissions $user_id $project_id view read write admin
if {$admin} { set perm_p 1 }

# Users with "add_projects" privilege can always create new projects...
if {[im_permission $user_id add_projects]} { set perm_p 1 } 
if {!$perm_p} { 
    ad_return_complaint "Insufficient Privileges" "
        <li>You don't have sufficient privileges to see this page."
    return
}

set return_url [export_vars -base "/intranet/projects/view" {project_id}]
set project_url $return_url

# Check that we have logged hours
set logged_hours_p [db_string logged_hours "select 1 from im_hours where project_id in (select project_id from im_projects where parent_id = :project_id union select :project_id from dual) limit 1" -default 0]

if {!$logged_hours_p} {
    ad_return_error "Missing hours" "You did not log hours! Please do this first"
} {
    # Set project status to delivered
    set project_status_id [im_project_status_delivered]
    db_dml update_project_status "update im_projects set project_status_id = :project_status_id where project_id = :project_id"

    db_1row project_info "select project_type_id, project_status_id from im_projects where project_id = :project_id"
    
    # Write Audit Trail
    im_project_audit -project_id $project_id -type_id $project_type_id -status_id $project_status_id -action after_update
    
}
            

ad_returnredirect $return_url
