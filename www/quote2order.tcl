#
#
# Copyright (c) 2011, cognovís GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
#
 
ad_page_contract {
    
    Turns a quote into an order for a translation project.
    
    Primarily designed for a customer to accept the project
    
    @author <yourname> (<your email>)
    @creation-date 2012-03-11
    @cvs-id $Id$
} {
    {project_id ""}
    {invoice_id ""}
} -properties {
} -validate {
} -errors {
}

# -----------------------------------------------------------
# Defaults
# -----------------------------------------------------------
set user_id [im_require_login -no_redirect_p 1]

set complaint ""

if {$project_id eq ""} {
    # Get the project_id from the invoice
    set project_id [db_string project "select project_id from im_costs where cost_id = :invoice_id" -default ""]
}

if {$project_id eq ""} {
	set complaint "Can't find project"
}

# Check if the user has admin rights on the parent_id
# to allow freelancers to add sub-projects
set perm_p 0
im_project_permissions $user_id $project_id view read write admin
if {$admin} { set perm_p 1 }
	
# Users with "add_projects" privilege can always create new projects...
if {[im_permission $user_id add_projects]} { set perm_p 1 }

# Quote recipient can also accept
set company_contact_id [db_string quote "select company_contact_id from im_invoices i, im_costs c, acs_objects o where cost_type_id = [im_cost_type_quote] and project_id = :project_id and i.invoice_id = c.cost_id order by o.last_modified desc limit 1" -default ""]

if {$company_contact_id eq $user_id} { set perm_p 1}

if {!$perm_p} {
    set complaint "Insufficient Privileges <li>You don't have sufficient privileges to see this page."
}

# Get all information about the quote and customer and project
db_1row customer_and_project_info "select
	project_status_id,
	project_type_id,
	project_nr,
	project_lead_id,
	c.company_id,
	p.start_date,
	p.end_date,
	to_char(now(),'YYYY-MM-DD') as new_start_date,
	to_char(now(),'HH24') as start_hour,
	company_project_nr,
	company_status_id,project_cost_center_id as cost_center_id
from im_projects p, im_companies c
where p.company_id = c.company_id
and p.project_id = :project_id"

if {[lsearch [im_sub_categories [im_project_status_potential]] $project_status_id] <0} {
    set complaint "Your project $project_nr is no longer in status Potential but [im_category_from_id $project_status_id]. Can't accept the project in this case."
}

db_transaction {
    
    set start_timestamp [db_string start_date "select to_char(now(),'YYYY-MM-DD HH24:MI') from dual"]
    
    im_translation_update_project_dates -project_id $project_id -start_timestamp $start_timestamp -group_languages
    
    
    # ---------------------------------------------------------------
    # Update the project information
    # ---------------------------------------------------------------
    
    db_dml update_project_info "update im_projects set project_status_id = [im_project_status_open] where project_id = :project_id"
    
    
    # Set the customer to active if not already done so
    
    if {[im_company_status_active] != $company_status_id} {
	db_dml make_active "update im_companies set company_status_id = [im_company_status_active] where company_id = :company_id"
    }
    
    
    # ---------------------------------------------------------------
    # Accept the latest quote and decline all others
    # ---------------------------------------------------------------
    set quote_id [db_string quote "select cost_id from im_costs c, acs_objects o where cost_type_id = [im_cost_type_quote] and project_id = :project_id order by o.last_modified desc limit 1" -default ""]
    
    if {$quote_id ne ""} {
	db_dml accept_quote "update im_costs set cost_status_id = [im_cost_status_accepted] where cost_id = :quote_id"
	db_foreach unaccepted_quote "select cost_id as declined_quote_id from im_costs where cost_type_id = [im_cost_type_quote] and cost_status_id = [im_cost_status_created] and project_id = :project_id and cost_id != :quote_id" {
	    db_dml decline_quote "update im_costs set cost_status_id = [im_cost_status_filed] where cost_id = :declined_quote_id"
	}
    }
    
    
    # Write Audit Trail
    im_project_audit -project_id $project_id -type_id $project_type_id -status_id [im_project_status_open] -action after_update
}

set project_url [export_vars -base "/intranet/projects/view" -url {project_id}]

if {$user_id eq $company_contact_id} {
    # ---------------------------------------------------------------
    # Notify PM about acceptance
    # ---------------------------------------------------------------
    
    
    im_freelance_notify \
	-object_id $project_id \
	-recipient_ids $project_lead_id \
	-message "Project <a href='$project_url'>$project_nr</a> has been accepted" \
	-severity 0
    
    
    # Confirmation for the company_contact?
    set end_date_pretty [lc_time_fmt [db_string end_time "select end_date from im_projects where project_id = :project_id" -default ""] "%Q %X" [lang::user::site_wide_locale -user_id $company_contact_id]]
    
    if {$quote_id ne ""} {
	set quote_nr [db_string quote_nr "select invoice_nr from im_invoices where invoice_id = :quote_id" -default ""]
    }
    
} else {
    # ---------------------------------------------------------------
    # If a PM accepted this, check for new files
    # ---------------------------------------------------------------
    
    set new_files_p 0
    if {[apm_package_installed_p intranet-trados] && [im_trans_trados_project_p -project_id $project_id]} {
	set new_files_p [im_trans_trados_new_file_p -project_id $project_id]
	set new_file_url [export_vars -base "/intranet-trans-trados/update-tmbs-from-company" -url {project_id}]
    }
}


